/*jshint esversion:6*/
/*jshint asi:true*/
const setupEvents = require('./installers/setupEvents')
if (setupEvents.handleSquirrelEvent()) {return;}
const {app,globalShortcut,Tray,Menu,ipcMain,dialog,BrowserWindow,clipboard} = require('electron')
const path = require('path')
const url = require('url')
const AutoLaunch = require('auto-launch');

var fmAutoLauncher = new AutoLaunch({
    name: 'FastMoji',
    path: path.join(__dirname, 'FastMoji.exe'),
    isHidden: true
});

fmAutoLauncher.enable();

let mainWindow = null
let tray = null
var hidden = true;

const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
        if (mainWindow.isMinimized()) {
            mainWindow.restore()
            mainWindow.focus()
        }
    }
})

if (shouldQuit) {
    app.quit()
}

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 650,
        height: 400,
        resizable: false,
        transparent: true,
        frame: false,
        alwaysOnTop: true,
        icon: path.join(__dirname, 'assets/icons/win/icon.ico')
    })

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    mainWindow.on('minimize', (event) => {
        event.preventDefault();
        mainWindow.hide();
    });

    mainWindow.on('close', (event) => {
        if (!app.isQuiting) {
            event.preventDefault()
            mainWindow.hide();
        }
        return false;
    });

    mainWindow.on('show', () => {
        hidden = false
    })
    mainWindow.on('hide', () => {
        hidden = true
    })
}

function showOrCreate() {
    if (mainWindow === null) {
        createWindow()
        mainWindow.show()
        hidden = false
        let esc = globalShortcut.register('Esc', () => {
            globalShortcut.unregister('Esc')
            mainWindow.hide()
            hidden = true;
        })
    } else {
        if (mainWindow.isVisible()) {
            mainWindow.hide()
        } else {
            createWindow()
            mainWindow.show()
            hidden = false
            let esc = globalShortcut.register('Esc', () => {
                globalShortcut.unregister('Esc')
                mainWindow.hide()
                hidden = true;
            })
        }
    }
}

function traySetup() {
    tray = new Tray(path.join(__dirname, 'assets/icons/win/icon.ico'));
    tray.on('click', () => {
        showOrCreate()
    })
    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Open FastMoji',
            click: () => {
                if (mainWindow === null) {
                    createWindow()
                    mainWindow.show()
                    hidden = false
                    let esc = globalShortcut.register('Esc', () => {
                        globalShortcut.unregister('Esc')
                        mainWindow.hide()
                        hidden = true;
                    })
                } else {
                    showOrCreate()
                }
            }
        }, {
            label: 'About',
            click: () => {
                if (mainWindow === null) {
                    createWindow()
                }
                dialog.showMessageBox(mainWindow, {
                    type: 'info',
                    title: 'About',
                    message: 'FastMoji is built by AyymericM',
                    detail: 'You can find him on GitHub and Twitter as @AyymericM, or on his website, aymericm.fr',
                    icon: path.join(__dirname, 'assets/icons/win/icon.ico')
                });
            }
        }, {
            label: 'Help',
            click: () => {
                if (mainWindow === null) {
                    createWindow()
                }
                dialog.showMessageBox(mainWindow, {
                    type: 'info',
                    title: 'Help',
                    message: 'To open FastMoji, just hit Ctrl+Maj+E',
                    detail: 'To close the window, just hit Escape',
                    icon: path.join(__dirname, 'assets/icons/win/icon.ico')
                });
            }
        }, {
            type: 'separator'
        }, {
            label: 'Quit FastMoji',
            click: () => {
                app.quit();
            }
        }
    ]);
    tray.setToolTip('FastMoji');
    tray.setContextMenu(contextMenu);
}

app.on('ready', () => {
    const showSC = globalShortcut.register('CommandOrControl+Shift+E', () => {
        if (hidden === true) {
            createWindow()
            mainWindow.show()
            hidden = false;
            globalShortcut.register('Esc', () => {
                globalShortcut.unregister('Esc')
                mainWindow.hide()
                hidden = true;
            })
        }
    })
    traySetup()
})

app.on('before-quit', () => {
    globalShortcut.unregisterAll()
    mainWindow.removeAllListeners('close')
    app.exit()
})

app.on('window-all-closed', function() {
    globalShortcut.unregister('Esc')
})

app.on('activate', function() {
    if (mainWindow === null) {
        createWindow()
    }
})

ipcMain.on('emoji', (event, arg) => {
    clipboard.writeText(arg, "selection");
    event.sender.send('copy-done', true);
});

ipcMain.on('hide', (event, arg) => {
    mainWindow.hide()
    hidden = true;
});
