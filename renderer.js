/*jshint esversion:6*/
/*jshint asi:true*/

const emoji = require('emoji.json')
const Fuse = require('./js/fuse.min.js')
var {ipcRenderer, remote} = require('electron')
var main = remote.require("./main.js")

var options = {
	shouldSort: true,
	threshold: 0.6,
	location: 0,
	distance: 100,
	maxPatternLength: 32,
	minMatchCharLength: 3,
	keys: ["keywords"]
}

function showAll() {
	$('#list').html('')
	for (var i = 0; i < emoji.length; i++) {
		$("#list").append('<div id="' + i + '" class="list-item"><div class="list-item-emoji" data="' + emoji[i].char + '">' + emoji[i].char + '</div><div class="list-item-alias">' + emoji[i].name.toLowerCase().split("≊").pop() + '</div><div class="list-item-scut">' + i + '</div></div>')
	}
}

$(document).ready(function() {
	showAll()
	$('input').focus()
	enableListener()
})

function sendEmoji(emoji) {
	ipcRenderer.send('emoji', emoji)
}

$('input').keyup(function(e) {
	if (e.keyCode >= 48 && e.keyCode <= 57) {
		e.preventDefault();
        $('input').val(null);
		sendEmoji($('#' + (e.keyCode - 48)).children('.list-item-emoji').attr('data'));
    } else if (e.keyCode >= 96 && e.keyCode <= 105) {
        e.preventDefault();
        $('input').val(null);
		sendEmoji($('#' + (e.keyCode - 96)).children('.list-item-emoji').attr('data'));
	} else {
		var keyword = $("input").val()
		$('#list').html('')
		var fuse = new Fuse(emoji, options)
		var result = fuse.search(keyword);
		if (result.length > 0) {
			for (var i = 0; i < result.length; i++) {
				$("#list").append('<div id="' + i + '" class="list-item"><div class="list-item-emoji" data="' + result[i].char + '">' + result[i].char + '</div><div class="list-item-alias">' + result[i].name.toLowerCase().split("≊").pop() + '</div><div class="list-item-scut">' + i + '</div></div>')
			}
		} else {
			showAll()
		}
		enableListener()
	}
})

function enableListener() {
	$('.list-item').click(function(e) {
		e.preventDefault()
		var val = $(this).children('.list-item-emoji').attr('data')
		sendEmoji(val)
		$('input').val(null)
		$('input').focus()
	})
}

ipcRenderer.on('copy-done', function(event, arg) {
	if (arg === true) {
		$('input').attr('placeholder', 'Copied to clipboard ! ✓')
		setTimeout(function() {
			$('input').attr('placeholder', 'Search for an emoji...')
			ipcRenderer.send('hide')
		}, 2000)
	}
})
