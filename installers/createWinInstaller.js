/*jshint esversion:6*/
/*jshint asi:true*/
const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')

getInstallerConfig()
     .then(createWindowsInstaller)
     .catch((error) => {
     console.error(error.message || error)
     process.exit(1)
 })

function getInstallerConfig () {
    console.log('creating windows installer')
    const rootPath = path.join('./')
    const outPath = path.join(rootPath, 'release-builds')

    return Promise.resolve({
       appDirectory: path.join(outPath, 'FastMoji-win32-ia32/'),
       authors: 'AymericM',
       noMsi: true,
       outputDirectory: path.join(outPath, 'windows-installer'),
       exe: 'FastMoji.exe',
       setupExe: 'FastMojiInstaller.exe',
       setupIcon: path.join(rootPath, 'assets', 'icons', 'win', 'icon.ico'),
       loadingGif: path.join(rootPath, 'installers', 'loading.gif')
   })
}
